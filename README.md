# TagClusterMap
タグのクラスター表示のサンプル

B!側のアップ先 →　https://gitlab.com/robot_kanji/tagmap-server/-/tree/develop/viewer/

## A. JSON読み込み＋グラフ&クラスタ描画
d3.js v2 利用
- Sample2 https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample2/sample2.html

## B. JSON読み込み＋グラフ＆クラスタ描画（自動レイアウト）
A.に力学的バネモデル（自動レイアウト）を適用
- Sample1 https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample1/sample1.html

## C. JSON読み込み＋タグ描画
シンプルな散布図ライクな可視化
- Sample3 https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample3/sample3.html

JSONデータの仕様
```json
{
    "groups" : [
        {
            "id": 1,
            "name": "アニメ",
        },
        {
            ・・・中略・・・
        }
    ],
    "nodes": [
        {  
            "tag":"ポムポムプリン",  
            "group_id":1,
            "center":0.334,
            "x": 0.123,
            "y": 0.456
        },
        {  
            ・・・中略・・・
        }
    ]  
}  
```

## A+C. 合体バージョン
- Sample4 https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample4/sample4.html

- Sample6（BuddyUP!サンプルデータ取り込み） https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample6/sample6.html

- Sample7（ラベルの重なり緩和、クリックイベント確認） https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample7/sample7.html
  
  
## B+C. 合体バージョン＋ユーザー表示
- Sample5 https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sample5/sample5.html

## サンドボックス
- https://github.labs.fujitsu.com/pages/ishihara-masaki/TagClusterMap/sandbox/helloworld.html

- 練習用サンプルデータ
    ```json
    [
        {id:1,label:"h",v:45},
        {id:2,label:"e",v:20},
        {id:3,label:"l",v:40},
        {id:4,label:"l",v:89},
        {id:5,label:"o",v:85},
        {id:6,label:"w",v:100},
        {id:7,label:"o",v:25},
        {id:8,label:"r",v:60},
        {id:9,label:"l",v:90},
        {id:10,label:"d",v:70}
    ]
    ```
- 練習用スタイルシート
    ```css
        div.chart{
            font-family:sans-serif;
            font-size:0.7em;
        }
        div.bar{
            background-color:DarkRed;
            color:white;
            height:3em;
            line-height:3em;
            padding-right:1em;
            margin-bottom:2px;
            text-align:right;
        }
    ```
- 練習用コード
    ```javascript
    d3.select("body")
        .append("div")
            .attr("class","chart")
        .selectAll(".bar")
        .data(data)
        .enter()
        .append("div")
            .attr("class","bar")
            .style("width", function(d){return d.v + "px"})
            .style("outline","1px solid black")
            .text(function(d){return d.label});
    ```

## 参考
- http://jsfiddle.net/nrabinowitz/9a7yy/
- http://jsfiddle.net/nrabinowitz/yPfJH/

以上
var width = 700, height = 500;
var margin = { "top": 30, "bottom": 60, "right": 30, "left": 60 };
var radius = 60;
var svg = d3.select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height + 300);

var nodeContainer = svg.append('g'),
    textContainer = svg.append('g');

function circleId(d){ return 'circle' + d.tag; }
function maskId(d){ return 'mask' + d.tag; }

// データ読み込み
d3.json("sample.json")
.then(function(json){
    
    // 軸スケールの設定
    var xScale = d3.scaleLinear()
        .domain([0, d3.max(json.nodes, function(d) { return d.x; })])
        .range([margin.left, width - margin.right]);
    var yScale = d3.scaleLinear()
        .domain([0, d3.max(json.nodes, function(d) { return d.y; })])
        .range([height - margin.bottom, margin.top]);

    // 軸の表示
    var axisx = d3.axisBottom(xScale).ticks(10);
    var axisy = d3.axisLeft(yScale).ticks(10);
    svg.append("g")
        .attr("transform", "translate(" + 0 + "," + (height - margin.bottom) + ")")
        .call(axisx)
        .append("text")
        .attr("class", "axis-label")
        .attr("x", (width - margin.left - margin.right) / 2 + margin.left)
        .attr("y", 35)
        .attr("text-anchor", "middle")
        .text("X Label");
    svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + 0 + ")")
        .call(axisy)
        .append("text")
        .attr("class", "axis-label")
        .attr("x", -(height - margin.top - margin.bottom) / 2 - margin.top)
        .attr("y", -35)
        .attr("transform", "rotate(-90)")
        .attr("text-anchor", "middle")
        .text("Y Label");

    // 準備
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    var groupMap = d3.nest()
        .key(function(d){ return d.group_id; })
        .map(json.nodes);

    // クラスターの表示
    var node = nodeContainer
        .selectAll(".node")
        .data(json.nodes)
        .enter().append("g")
        .attr("class", "node");
    node.append("defs")
        .append("circle")
        .attr("id", circleId)
        .attr("r", radius);
    var mask = node.append("mask")
        .attr("id", maskId);
    mask.append("use")
        .attr("xlink:href", function(d){ return "#" + circleId(d); })
        .attr("fill", "white")
        .attr("stroke-width", 2)
        .attr("stroke", "white");
    mask.selectAll("use.other")
        .data(function(d){
            return groupMap["$" + d.group_id].filter(function(other){
                return other.tag != d.tag;
            })
        })
        .enter().append("use")
        .attr("class", "other")
        .attr("xlink:href", function(d){ return "#" + circleId(d); })
        .attr("fill", "black");
    node.append("use")
        .attr("xlink:href", function(d){ return "#" + circleId(d); })
        .attr("mask", function(d){ return "url(#" + maskId(d) + ")"; })
        .style("fill", "white")
        .style("stroke", function(d){ return color(d.group_id); });

    //タグの表示
    textContainer.selectAll("text")
        .data(json.nodes)
        .enter()
        .append("text")
        .attr("dx", 2)
        .attr("dy", ".35em")
        .attr("text-anchor", "middle")
        .text(function (d) { return d.tag })
        .style('fill', function(d) { return color(d.group_id) }); 

    //表示位置
    nodeContainer.selectAll("circle")
        .attr("transform", function(d){ return "translate(" + xScale(d.x) + "," + yScale(d.y) +")"});
    textContainer.selectAll('text')
        .attr("transform", function(d) { return "translate(" + xScale(d.x) + "," + yScale(d.y) +")"});

})
.catch(function(error){
  // エラー処理

});


var width = 700, height = 500;
var margin = { "top": 30, "bottom": 60, "right": 30, "left": 60 };
var svg = d3.select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height + 300);

// データ読み込み
d3.json("sample.json")
.then(function(json){
    
    // 軸スケールの設定
    var xScale = d3.scaleLinear()
        .domain([0, d3.max(json.nodes, function(d) { return d.x; })])
        .range([margin.left, width - margin.right]);
    var yScale = d3.scaleLinear()
        .domain([0, d3.max(json.nodes, function(d) { return d.y; })])
        .range([height - margin.bottom, margin.top]);

    // 軸の表示
    var axisx = d3.axisBottom(xScale).ticks(0.1);
    var axisy = d3.axisLeft(yScale).ticks(0.1);
    svg.append("g")
        .attr("transform", "translate(" + 0 + "," + (height - margin.bottom) + ")")
        .call(axisx)
        .append("text")
        .attr("class", "axis-label")
        .attr("x", (width - margin.left - margin.right) / 2 + margin.left)
        .attr("y", 35)
        .attr("text-anchor", "middle")
        .text("X Label");
    svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + 0 + ")")
        .call(axisy)
        .append("text")
        .attr("class", "axis-label")
        .attr("x", -(height - margin.top - margin.bottom) / 2 - margin.top)
        .attr("y", -35)
        .attr("transform", "rotate(-90)")
        .attr("text-anchor", "middle")
        .text("Y Label");

    // タグ（円、テキスト）の表示
    svg.append("g")
        .selectAll(".dot")
        .data(json.nodes)
        .enter()
        .append("circle")
        .attr("class", "dot")
        .attr("cx", function(d) { return xScale(d.x); })
        .attr("cy", function(d) { return yScale(d.y); })
        .attr("fill", "steelblue")
        .attr("r", 4);
    svg.append("g")
        .selectAll(".label")
        .data(json.nodes)
        .enter()
        .append("text")
        .attr("class", "label")
        .attr("x", function(d) { return xScale(d.x); })
        .attr("y", function(d) { return yScale(d.y); })
        .attr("dy", "1em")
        .text(function(d){ return d.tag});
})
.catch(function(error){
  // エラー処理

});


var width = 700, height = 500;
var color = d3.scale.category20();
var force = d3.layout.force().gravity(.05).distance(100).charge(-100).size([width, height + 300]);

var svg = d3.select("body").append("svg").attr("width", width).attr("height", height + 300);

var nodeContainer = svg.append('g'),
    linkContainer = svg.append('g'),
    textContainer = svg.append('g');

queue()
	.defer(d3.json, 'sample.json')
    .await(makeMap);

function makeMap(json){
    force
        .nodes(json.nodes)
        .links(json.links);

    // compute a static layout
    force.start();
    for (var i = 0; i < 500; i++) force.tick();
    force.stop();

    var link = linkContainer.selectAll(".link")
        .data(json.links)
        .enter().append("line")
        .attr("class", "link");

    var node = nodeContainer.selectAll(".node")
        .data(json.nodes)
        .enter().append("g")
        .attr("class", "node");

    var color = d3.scale.category10();

    // function to make a circle id from node data
    function circleId(d) {
        return 'circle' + d.name
    }
    function maskId(d) {
        return 'mask' + d.name
    }

    // make an array of group names
    var groupMap = d3.nest()
        .key(function(d) { return d.group; })
        .map(json.nodes);

    
    // put the circle in as a non-rendered definition
    node.append('defs').append('circle')
        .attr('id', circleId)
        .attr('r', function(d) { return parseInt(d.fontsize.replace('px', '')) * 2 });

    // make mask using all other circles in this group
    var mask = node.append('mask')
        .attr('id', maskId);
                                    
    mask.append('use')
        .attr('xlink:href', function(d) { return '#' + circleId(d); })
        .attr('fill', 'white')
        .attr('stroke-width', 2)
        .attr('stroke', 'white');
                                    
    mask.selectAll('use.other')
        .data(function(d) {
            return groupMap[d.group].filter(function(other) {
                return other.name != d.name;
            });
        })
        .enter().append('use')
        .attr('class', 'other')
        .attr('xlink:href', function(d) { return '#' + circleId(d); })
        .attr('fill', 'black');
                                        
    node.append('use')
        .attr('xlink:href', function(d) { return '#' + circleId(d); })
        .attr('mask', function(d) { return 'url(#' + maskId(d) + ')'; })
        .style('fill', 'white')
        .style('stroke', function(d) { return color(d.group) });

    textContainer.selectAll('text')
        .data(json.nodes)
        .enter().append("text")
        .attr("dx", 2)
        .attr("dy", ".35em")
        .attr("text-anchor", "middle")
        .attr("style", function (d) { return "font-size:" + d.fontsize })
        .text(function (d) { return d.name })
        .style('fill', function(d) { return color(d.group) }); 
                                        
        // force.on("tick", function () {
        link
            .attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });
        
        // reposition circle defs
        nodeContainer.selectAll('circle')
            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y +")"});
        
        // reposition text
        textContainer.selectAll('text')
            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y +")"});
        // });            
}
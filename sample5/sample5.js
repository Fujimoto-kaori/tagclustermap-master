var width = 700, height = 500;
var margin = { "top": 30, "bottom": 60, "right": 30, "left": 60 };
var radius = 60;
var color = d3.scaleOrdinal(d3.schemeCategory10);
var svg = d3.select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height + 300);
var nodeContainer = svg.append('g'),
    linkContainer = svg.append('g'),
    textContainer = svg.append('g');
var simulation = d3.forceSimulation()
function circleId(d){ return 'circle' + d.name; }
function maskId(d){ return 'mask' + d.name; }
function dragstarted(d) {
    if(!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}
function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}
function dragended(d) {
    if(!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}

// データ読み込み
d3.json("sample.json")
.then(function(json){
   

    // 軸スケールの設定
    var xScale = d3.scaleLinear()
        .domain([0, d3.max(json.tags, function(d) { return d.x; })])
        .range([margin.left, width - margin.right]);
    var yScale = d3.scaleLinear()
        .domain([0, d3.max(json.tags, function(d) { return d.y; })])
        .range([height - margin.bottom, margin.top]);

    // 軸の表示
    var axisx = d3.axisBottom(xScale).ticks(10);
    var axisy = d3.axisLeft(yScale).ticks(10);
    svg.append("g")
        .attr("transform", "translate(" + 0 + "," + (height - margin.bottom) + ")")
        .call(axisx)
        .append("text")
        .attr("class", "axis-label")
        .attr("x", (width - margin.left - margin.right) / 2 + margin.left)
        .attr("y", 35)
        .attr("text-anchor", "middle")
        .text("X Label");
    svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + 0 + ")")
        .call(axisy)
        .append("text")
        .attr("class", "axis-label")
        .attr("x", -(height - margin.top - margin.bottom) / 2 - margin.top)
        .attr("y", -35)
        .attr("transform", "rotate(-90)")
        .attr("text-anchor", "middle")
        .text("Y Label");

    // 準備
    var groupMap = d3.nest()
        .key(function(d){ return d.group_id; })
        .map(json.tags);

    // ユーザの表示
    var user = linkContainer.selectAll(".user")
        .data(json.users)
        .enter()
        .append("circle")
        .attr("class", "user")
        .attr("r", 7)
        .attr("fill", "LightSalmon")
        .call(d3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));

    // リンクの表示
    var link = linkContainer.selectAll(".link")
        .data(json.links)
        .enter()
        .append("line")
        .attr("class", "link")
        .attr("stroke", "#000")
        .attr("stroke-weight", 1);

    // クラスターの表示
    var node = nodeContainer
        .selectAll(".node")
        .data(json.tags)
        .enter().append("g")
        .attr("class", "node");
    node.append("defs")
        .append("circle")
        .attr("id", circleId)
        .attr("r", radius)
        .attr("cx", function(d){ return xScale(d.x) })
        .attr("cy", function(d){ return yScale(d.y) });
    var mask = node.append("mask")
        .attr("id", maskId);
    mask.append("use")
        .attr("xlink:href", function(d){ return "#" + circleId(d); })
        .attr("fill", "white")
        .attr("stroke-width", 2)
        .attr("stroke", "white");
    mask.selectAll("use.other")
        .data(function(d){
            return groupMap["$" + d.group_id].filter(function(other){
                return other.name != d.name;
            })
        })
        .enter().append("use")
        .attr("class", "other")
        .attr("xlink:href", function(d){ return "#" + circleId(d); })
        .attr("fill", "black");
    node.append("use")
        .attr("xlink:href", function(d){ return "#" + circleId(d); })
        .attr("mask", function(d){ return "url(#" + maskId(d) + ")"; })
        .style("fill", "white")
        .style("stroke", function(d){ return color(d.group_id); });

    //タグの表示
    textContainer.selectAll("text")
        .data(json.tags)
        .enter()
        .append("text")
        .attr("dx", 2)
        .attr("dy", ".35em")
        .attr("text-anchor", "middle")
        .attr("x", function(d){ return xScale(d.x) })
        .attr("y", function(d){ return yScale(d.y) })
        .text(function (d) { return d.name })
        .style('fill', function(d) { return color(d.group_id) }); 


    //表示位置
    /*
    nodeContainer.selectAll("circle")
        .attr("transform", function(d){ return "translate(" + xScale(d.x) + "," + yScale(d.y) +")"});
    textContainer.selectAll('text')
        .attr("transform", function(d){ return "translate(" + xScale(d.x) + "," + yScale(d.y) +")"});
    */


    //グラフ描画
    simulation
        .force("link", d3.forceLink())
        .force("charge", d3.forceManyBody())
        .force("center", d3.forceCenter(200, 150))
        .nodes(Object.assign(json.tags,json.users))
        .on("tick", ticked)
        .links(json.links);
    function ticked(){
        link
            .attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });
        user
            .attr("cx", function(d){ return d.x})
            .attr("cy", function(d){ return d.y});
        node
            .attr("cx", function(d){ return d.x})
            .attr("cy", function(d){ return d.y});
    }

})
.catch(function(error){
  // エラー処理

});